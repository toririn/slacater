module Slacater
  module TeamDecorator
    def slack_url
      "https://#{self.domain}.slack.com/"
    end
  end
end
