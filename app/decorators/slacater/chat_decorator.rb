module Slacater
  module ChatDecorator
    def display(processor = nil)
      return self.text if processor.blank?
      # processor: check for https://github.com/rutan/slack_markdown#usage
      processor.call(self.text)[:output].to_s
    end
  end
end
