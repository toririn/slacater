module Slacater
  module Modules
    module TeamBase
      extend ActiveSupport::Concern

      included do
        self.abstract_class = true
        belongs_to :team

        scope :by_team_id, ->(team_id) { where(team_id: team_id) }
      end
    end
  end
end
