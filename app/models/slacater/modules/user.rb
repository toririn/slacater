module Slacater
  module Modules
    module User
      extend ActiveSupport::Concern

      included do
        has_many :channel_users
        has_many :channels, through: :channel_users
        def self.by_slack(param = nil, id: nil, name: nil)
          if id.present?
            self.find_by(slack_id: id)
          elsif name.present?
            self.find_by(slack_name: name)
          elsif param.present?
            self.find_by(slack_id: param) || self.find_by(slack_name: param)
          end
        end
      end
    end
  end
end
