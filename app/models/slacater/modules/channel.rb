module Slacater
  module Modules
    module Channel
      extend ActiveSupport::Concern

      included do
        belongs_to :channel_kind, optional: true
        has_many :chats
        has_many :channel_users
        has_many :users, through: :channel_users

        scope :with_channel_kind, ->{ includes(:channel_kind) }
      end
    end
  end
end
