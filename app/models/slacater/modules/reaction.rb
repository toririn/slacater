module Slacater
  module Modules
    module Reaction
      extend ActiveSupport::Concern

      included do
        def reaction?
          true
        end
      end
    end
  end
end
