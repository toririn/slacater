module Slacater
  module Modules
    module Base
      extend ActiveSupport::Concern

      included do
        self.abstract_class = true

        arel = self.arel_table
        def slacater_model?
          true
        end
      end
    end
  end
end
