module Slacater
  module Modules
    module ChatUserReaction
      extend ActiveSupport::Concern

      included do
        has_one :chat
        belongs_to :user
      end
    end
  end
end
