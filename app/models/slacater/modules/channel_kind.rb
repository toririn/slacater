module Slacater
  module Modules
    module ChannelKind
      extend ActiveSupport::Concern

      included do
        has_many :channels
      end
    end
  end
end
