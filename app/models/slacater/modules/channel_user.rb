module Slacater
  module Modules
    module ChannelUser
      extend ActiveSupport::Concern

      included do
        belongs_to :channel
        belongs_to :user
      end
    end
  end
end
