module Slacater
  module Modules
    module Team
      extend ActiveSupport::Concern

      SECURE = ENV['SLACATER_SECURE_TOKEN']
      CIPHER = ENV['SLACATER_CIPHER_TOKEN']

      included do

        has_many :users
        has_many :channels
        has_many :chats

        scope :authenticated,   -> { where.not(slack_id: nil, domain: nil) }
        scope :unauthenticated, -> { where(slack_id: nil, domain: nil) }

        before_save :encrypt_columns

        def self.by_slack(param = nil, id: nil, name: nil)
          if id.present?
            self.find_by(slack_id: id)
          elsif name.present?
            self.find_by(name: name)
          elsif param.present?
            self.find_by(slack_id: param) || self.find_by(name: param)
          end
        end

        def self.encrypt(value)
          ActiveSupport::MessageEncryptor.new(SECURE, CIPHER).encrypt_and_sign(value)
        end

        def self.decrypt(value)
          ActiveSupport::MessageEncryptor.new(SECURE, CIPHER).decrypt_and_verify(value)
        end

        def authenticated?
          self.name.present? && self.domain.present? && self.slack_id.present?
        end

        def decrypt_token
          crypt.decrypt_and_verify(self.token)
        end

        def decrypt_client_id
          crypt.decrypt_and_verify(self.client_id)
        end

        def decrypt_secret_key
          crypt.decrypt_and_verify(self.secret_key)
        end

        def slack_connect_value
          @slack_connect_value ||= begin
            Slacater::SlackConnectValue.new(
              client_id:    self.decrypt_client_id,
              secret_key:   self.decrypt_secret_key,
              scope:        self.scope,
              team_name:    self.name,
              redirect_uri: self.redirect_uri,
              state:        crypt.encrypt_and_sign(self.id),
            )
          end
        end
        private

        def encrypt_columns
          self.token      = crypt.encrypt_and_sign(self.token)
          self.client_id  = crypt.encrypt_and_sign(self.client_id)
          self.secret_key = crypt.encrypt_and_sign(self.secret_key)
        end

        def crypt
          ActiveSupport::MessageEncryptor.new(SECURE, CIPHER)
        end
      end
    end
  end
end
