module Slacater
  module Modules
    module ChatReaction
      extend ActiveSupport::Concern

      included do
        def chat_reaction?
          true
        end
      end
    end
  end
end
