module Slacater
  module Modules
    module Chat
      extend ActiveSupport::Concern

      included do
        belongs_to :channel
        belongs_to :user

        arel = self.arel_table

        scope :with_teams,         -> { includes(:team, :channel, :user) }


        scope :by_user_ids_or_all,    ->(user_ids) { user_ids.present? ? where(user_id: user_ids) : all }
        scope :by_channel_ids_or_all, ->(channel_ids) { channel_ids.present? ? where(channel_id: channel_ids) : all }
        scope :by_text_or_all,        ->(text) { text.present? ? where(arel[:text].matches("%#{text}%")) : all }
        scope :by_posted_term,        ->(posted_term) { where(posted_at: posted_term) }

        # テキストの値を検索。検索は部分一致、前方一致、広報一致で選べる。
        # いけてないので要リファクタリング
        scope :by_text_matches,  -> (text, type = :around) {
          p, s = mathes_type(type)
          where(arel[:text].matches(p + text + s))
        }
        scope :by_texts_matches, -> (texts, type = :around) {
          return by_text_matches(texts, type) if texts.is_a?(String)

          case texts.size
          when 0
            all
          when 1
            by_text_matches(texts.first, type)
          when 2
            by_text_matches(texts.first, type).or(by_text_matches(texts.second, type))
          when 3
            by_text_matches(texts.first, type).or(by_text_matches(texts.second, type)).or(by_text_matches(texts.third, type))
          else
            raise 'over texts size. for 0..3.'
          end
        }

        private

        def self.mathes_type(type)
          case type
          when :suffix
            ['%', '']
          when :prefix
            ['', '%']
          else
            ['%', '%']
          end
        end
      end
    end
  end
end
