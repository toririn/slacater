module Slacater
  class SlackConnectValue < BaseValue

    def initialize(client_id:, secret_key:, scope:, team_name:, redirect_uri:, state:)
      @client_id     = client_id
      @secret_key    = secret_key
      @scope         = scope
      @team_name     = team_name
      @redirect_uri  = redirect_uri
      @state         = state
    end

    def to_values
      { auth_path: {}, get_info_path: { code: 'code' } }
    end

    def auth_path
      "#{auth_host}client_id=#{@client_id}&state=#{@state}&scope=#{@scope}&team=#{@team_name}&redirect_uri=#{@redirect_uri}"
    end

    def get_info_path(code)
      path = "#{acs_host}client_id=#{@client_id}&client_secret=#{@secret_key}&code=#{code}"
    end

    private

    def auth_host
      'https://slack.com/oauth/authorize?'
    end

    def acs_host
      'https://slack.com/api/oauth.access?'
    end
  end
end
