require 'rails_helper'

RSpec.describe Slacater::Team do

  describe "#encrypt" do
   let(:team) { build(:team, token: "xoxp-11", client_id: "client_id", secret_token: "secret") }
   before do
     team.save
   end
   it 'token, client_id, secret_token, is encrypt' do
     expect(team.token).not_to be_blank
    end
  end
end
