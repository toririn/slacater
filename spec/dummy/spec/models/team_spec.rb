require 'rails_helper'

RSpec.describe Slacater::Team do

  describe "#encrypt" do
    let(:team)       { build(:team, token: token, client_id: client_id, secret_key: secret_key) }
    let(:token)      { 'xoxp-1111122222333334444455555' }
    let(:client_id)  { '111112222233333' }
    let(:secret_key) { '123456789123456789123456789123456789123456789123456789123456789123456789123456789' }
    before do
      team.save
    end
    it 'token, client_id, secret_token, is encrypt' do
     expect(team.token).not_to be_blank
     expect(team.client_id).not_to be_blank
     expect(team.secret_key).not_to be_blank
     expect(team.token).not_to eq token
     expect(team.client_id).not_to eq client_id
     expect(team.secret_key).not_to eq secret_key
     expect(team.decrypt_token).to eq token
     expect(team.decrypt_client_id).to eq client_id
     expect(team.decrypt_secret_key).to eq secret_key
    end
  end
end
