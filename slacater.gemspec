$:.push File.expand_path("../lib", __FILE__)

# Maintain your gem's version:
require "slacater/version"

# Describe your gem and declare its dependencies:
Gem::Specification.new do |s|
  s.name        = "slacater"
  s.version     = Slacater::VERSION
  s.authors     = ["toririn"]
  s.email       = ["toririn.paftako@gmail.com"]
  s.homepage    = ""
  s.summary     = "slacat models"
  s.description = "slacat models"
  s.license     = "MIT"

  s.files = Dir["{app,config,db,lib}/**/*", "MIT-LICENSE", "Rakefile", "README.md"]

  s.add_dependency "rails", "~> 5.0.1"
  s.add_dependency "active_decorator"

  s.add_development_dependency "mysql2"
  s.add_development_dependency "rspec-rails"
  s.add_development_dependency "factory_girl_rails"
  s.add_development_dependency "pry-rails"
end
